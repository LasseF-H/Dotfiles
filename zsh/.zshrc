# Path to your oh-my-zsh installation.
export ZSH="/home/lasse/.oh-my-zsh"

# Set name of the theme to load. Optionally, if you set this to "random"
ZSH_THEME="agnoster"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  debian
  dirhistory
  colored-man-pages
  zsh-syntax-highlighting
  zsh-autosuggestions
)

source $ZSH/oh-my-zsh.sh

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias sage="schroot -c stretch sage"
alias start_polybar="MONITOR=$(polybar -m|tail -1|sed -e 's/:.*$//g') polybar example"
alias aptu="sudo apt update && sudo apt list --upgradeable && sudo apt upgrade && sudo apt autoremove"
alias ls='ls --color -h --group-directories-first'

#Custom functions
mem()
{                                                                                                      
    ps -eo rss,pid,euser,args:100 --sort %mem | grep -v grep | grep -i $@ | awk '{printf $1/1024 "MB"; $1=""; print }'
}
# Install Ruby Gems to ~/gems
export GEM_HOME="$HOME/gems"
export PATH="$HOME/gems/bin:$PATH"
