#!/bin/bash

#If directories doesn't exist create them
mkdir ~/.config/i3
mkdir ~/.config/polybar
mkdir ~/.config/rofi
mkdir ~/.config/gtk-3.0
mkdir ~/.local/share/fonts

#Use stow to symlink them to the configs in this directory
stow i3 -t ~/.config/i3
stow rofi -t ~/.config/rofi
stow polybar -t ~/.config/polybar
stow xorg -t ~/
stow zsh -t ~/
stow vim -t ~/
stow gtk -t ~/
stow wallpaper -t ~/
stow urxvt -t ~/
stow ranger -t ~/

#Copy fonts and other files
cp fonts/* ~/.local/share/fonts/
