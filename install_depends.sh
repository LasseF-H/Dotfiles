#!/bin/bash

#Install various Debian-Packages
sudo apt install i3 xorg vim vim-nox unicode-rxvt ranger rofi lxpolkit redshift udiskie fonts-font-awesome arc-theme papirus-icon-theme

#Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

#Install vundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

#Install i3-gaps
sudo sh others/i3-gaps-deb.sh

#Install SF-Mono Font
sudo cp -r others/SF-Mono-Font /usr/share/fonts

#Install pywal
pip3 install pywal

#Clear screen and print further setup instructions
clear
echo "Dependencies should now be installed now run 'install_config.sh' to finish setup"
