set nocompatible              " be iMproved, required
filetype off                  " required

"Vundle stuff
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'airblade/vim-gitgutter'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'termhn/i3-vim-nav'
Plugin 'spf13/vim-autoclose'
Plugin 'xuyuanp/nerdtree-git-plugin'
Plugin 'taglist.vim'
Plugin 'a.vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'dylanaraps/wal.vim'
Plugin 'tpope/vim-eunuch'
Plugin 'tpope/vim-fugitive'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'scrooloose/nerdtree'
Plugin 'ervandew/supertab'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'chrisbra/csv.vim'
call vundle#end()            " required
filetype plugin indent on    " required

"Set basic options
syntax on 
set number
set mouse=a
colorscheme wal

"Set custom keyboard shortcuts
noremap <C-d> :sh<cr>
map <F5> :!bear make run<CR>
map <F6> :!bear make debug<CR>
map <F7> :NERDTreeToggle<CR>

"Apply YCM FixIt
map <F4> :YcmCompleter FixIt<CR>

"vim-airline config
let g:airline_theme = 'wal'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline_powerline_fonts = 1
let g:airline#extensions#branch#enabled=1

"Taglist configuration
nmap <F8> :TlistToggle <CR>
nmap <F9> :TlistAddFilesRecursive . <CR>
let g:Tlist_Process_File_Always = 1
let Tlist_Use_Right_Window   = 1

"Make autocomplete work like one expects
let g:SuperTabDefaultCompletionType = "<c-n>"
inoremap <expr> <cr> ((pumvisible())?("\<C-y>"):("\<cr>"))

"Autocmd's
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
