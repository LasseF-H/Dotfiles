[colors]
background = ${xrdb:color0:#222}
background-alt = ${xrdb:color0:#222}
foreground = ${xrdb:color7:#222}
foreground-alt = ${xrdb:color7:#222}
primary = ${xrdb:color1:#222}
secondary = ${xrdb:color2:#222}
alert = ${xrdb:color3:#222}

[bar/example]
monitor = ${env:MONITOR:eDP-1}
width = 100%
height = 30
radius = 0
fixed-center = true
bottom=true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

border-size = 0 
border-color = #00000000

padding-left = 0
padding-right = 0  

module-margin-left = 2
module-margin-right = 2

font-0 = FontAwesome:size=12:antialias=true;3
font-1 = SFMono-Regular:size=12:antialias=true;3

modules-left = i3 
modules-center = player-mpris-simple
modules-right = alsa battery battery2 filesystem temperature cpu memory eth wlan vpn date 

cursor-click = pointer
cursor-scroll = ns-resize

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

format-mounted-prefix = " "
format-mounted-prefix-foreground = ${colors.secondary}
format-mounted-prefix-underline = ${colors.primary}
label-mounted =  %percentage_used%% 
label-mounted-underline = ${colors.primary}
format-unmounted-prefix = " "
format-unmounted-prefix-foreground = ${colors.secondary}
format-unmounted-prefix-underline = ${colors.primary}
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.background-alt}

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = ${colors.foreground} 
label-mode-background = ${colors.background}

; focused = Active workspace on focused monitor
label-focused =  %index%
label-focused-background = ${module/bspwm.label-focused-background}
label-focused-underline = ${colors.primary} 
label-focused-padding = ${module/bspwm.label-focused-padding}

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-padding = ${module/bspwm.label-occupied-padding}

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${colors.primary}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${module/bspwm.label-urgent-background}
label-urgent-padding = ${module/bspwm.label-urgent-padding}

; Separator in between workspaces
; label-separator = |


[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true

[module/xbacklight]
type = internal/xbacklight

format = <label> <bar>
label = BL

bar-width = 10
bar-indicator = |
bar-indicator-foreground = #fff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.foreground-alt}

[module/system-nvidia-bbswitch]
type = custom/script
exec = ~/.config/polybar/scripts/system-nvidia-bbswitch/system-nvidia-bbswitch.sh
interval = 5
click-left = "optirun -b none nvidia-settings -c :8"

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = ""
format-prefix-foreground = ${colors.secondary}
format-underline =  ${colors.primary}
label = "%percentage:2%%" 

[module/memory]
type = internal/memory
interval = 2
format-prefix = "  "
format-prefix-foreground = ${colors.secondary}
format-underline = ${colors.primary}
label = "%percentage_used%%"

[module/wlan]
type = internal/network
interface = wlp4s0
interval = 1

format-connected-prefix = ""
format-connected-prefix-foreground = ${colors.secondary}

format-connected = <ramp-signal> <label-connected>
format-connected-underline = ${colors.primary} 
label-connected = "%{A1:~/.config/rofi-wifi-menu/rofi-wifi-menu.sh:} %essid% %{A}"

format-disconnected =
label-disconnected = Wi-Fi disconnected

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-5 = 
ramp-signal-6 = 
ramp-signal-7 = 
ramp-signal-8 = 
ramp-signal-9 = 
ramp-signal-foreground = ${colors.foreground-alt}

[module/eth]
type = internal/network
interface = eno1
interval = 3.0

format-connected-underline = ${colors.primary} 
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.secondary}
label-connected =  %local_ip%

format-disconnected =

[module/date]
type = internal/date
interval = 5

date =
date-alt = "%Y-%m-%d "

time = "%H:%M "
time-alt = "%H:%M:%S "

format-prefix = "  "
format-prefix-foreground = ${colors.secondary}
format-underline = ${colors.primary}

label = %time% %date%

[module/alsa]
type = internal/alsa
format-volume = <label-volume> <bar-volume> 
label-volume = " "
label-volume-prefix = " "
label-volume-foreground = ${colors.secondary}

format-muted = <label-muted>  ──────────
format-muted-foreground = ${colors.secondary}
label-muted = "  "

bar-volume-width = 10
bar-volume-foreground-0 = ${colors.background-alt
bar-volume-foreground-1 = ${colors.background-alt
bar-volume-foreground-2 = ${colors.background-alt
bar-volume-foreground-3 = ${colors.background-alt
bar-volume-foreground-4 = ${colors.background-alt
bar-volume-foreground-5 = ${colors.background-alt
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-indicator-foreground = ${colors.primary}
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-fill-foreground = ${colors.secondary} 
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.secondary}

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98

label-charging = %percentage%%
label-discharging = %percentage%%
label-full = %percentage%%
format-charging = <animation-charging> <label-charging>
format-charging-underline = ${colors.primary}

format-discharging = <ramp-capacity> <label-discharging>
format-discharging-underline = ${colors.background} 

format-full-prefix = " "
format-full-prefix-foreground = ${colors.secondary}
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-foreground = ${colors.secondary}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-foreground = ${colors.secondary}
animation-charging-framerate = 750

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-foreground = ${colors.secondary}
animation-discharging-framerate = 750

[module/battery2]
type = internal/battery
battery = BAT1
adapter = AC
full-at = 98

label-charging = %percentage%%
label-discharging = %percentage%%
label-full = %percentage%%
format-charging = <animation-charging> <label-charging>
format-charging-underline = ${colors.primary}

format-discharging = <ramp-capacity> <label-discharging>
format-discharging-underline = ${colors.background} 

format-full-prefix = " "
format-full-prefix-foreground = ${colors.secondary}
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-foreground = ${colors.secondary}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-foreground = ${colors.secondary}
animation-charging-framerate = 750

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-foreground = ${colors.secondary}
animation-discharging-framerate = 750

[module/player-mpris-simple]
type = custom/script
exec = ~/Dotfiles/polybar/scripts/player-mpris-simple/player-mpris-simple.sh
interval = 1
click-left = playerctl previous
click-right = playerctl next
click-middle = playerctl play-pause

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 70

format = <ramp> <label>
format-underline = ${colors.primary}
format-warn = <ramp> <label-warn>
format-warn-underline = ${colors.alert}

label =  %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-3 = 
ramp-4 = 
ramp-foreground = ${colors.secondary}

[module/debianupdates]
type = custom/script
exec = ~/.config/polybar/scripts/debianupdates/debianupdates.sh
interval = 30
label-font = 3
format-prefix = " "
format-prefix-foreground = ${colors.secondary} 
format-underline = ${colors.primary}

[module/vpn]
type = custom/script
exec = echo VPN 
exec-if = pgrep -x openvpn
interval = 5
format-underline = ${colors.primary} 
format-prefix = "  "
format-prefix-foreground = ${colors.secondary} 

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over

[global/wm]
margin-top = 5
margin-bottom = 5

; vim:ft=dosini
