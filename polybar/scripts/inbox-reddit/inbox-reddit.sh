#!/bin/sh

url="www.reddit.com/message/inbox/.json?feed=cfa3f4607f361a99ac9b44db9260dd693ae23c7c&user=LasseF-H"
unread=$(curl -sf "$url" | jq '.["data"]["children"] | length')

case "$unread" in
    ''|*[!0-9]*)
	unread=0
esac;

if [ "$unread" -gt 0 ]; then
   echo "#1 $unread"
else
   echo "#2"
fi
